package beans;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Christopher Hyde
 * @dueDate December 2, 2018
 * @Class CST-235
 * @Instructor Mark Smithers
 * 
 * @about This class is a managed bean to handle orders.
 */

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name="Orders")
@ViewScoped()
public class Orders {

	//Properties
	List<Order> orders;
	
	public Orders() {
		//Array of dummy orders.
		Order[] dummyOrders = new Order[] {
				new Order("E1234", "AlphaTech TV", 139.99f, 1),
				new Order("LT321", "Acme Catapult", 49.99f, 1),
				new Order("BEV01", "Diet Xola", 7.49f, 5),
				new Order("U5982", "Poxie Paper Plates", 4.99f, 10)};
		
		//Initialize orders with a dummyOrder array
		orders = Stream.of(dummyOrders).collect(Collectors.toList());
	
	}
	
	//--- Getters and Setters ---

	/**
	 * @return the orders
	 */
	public List<Order> getOrders() {
		return orders;
	}

	/**
	 * @param orders the orders to set
	 */
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
	

	
	
}
