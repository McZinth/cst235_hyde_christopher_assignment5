package business;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;

/**
 * Message-Driven Bean implementation class for: OrderMessageService
 */
@MessageDriven(
		activationConfig = { @ActivationConfigProperty(
				propertyName = "destination", propertyValue = "java:/jms/queue/Order"), @ActivationConfigProperty(
				propertyName = "destinationType", propertyValue = "javax.jms.Queue")
		}, 
		mappedName = "java:/jms/queue/Order")
public class OrderMessageService implements MessageListener {

	@EJB
	OrdersBusinessService service;
    /**
     * Default constructor. 
     */
    public OrderMessageService() {
        // TODO Auto-generated constructor stub
    }
	
	/**
     * @see MessageListener#onMessage(Message)
     */
    public void onMessage(Message message) {
    	if(message instanceof ObjectMessage && message != null) {
    		System.out.println("Message Service: Order add to database");
    	}
    	
        if(message instanceof TextMessage && message != null) {
        	try {
				System.out.println(
						"Message Service: " + ((TextMessage)message).getText() 
						);
			} catch (JMSException e) {
				System.out.println("Message Service Error: Unable to print message!");
				e.printStackTrace();
			}
        }
        
    }

}
