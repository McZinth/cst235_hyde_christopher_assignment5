package business;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

import beans.Order;

/**
 * Session Bean implementation class AnotherOrdersBusinessService
 */
@Stateless
@Local(OrdersBusinessInterface.class)
@LocalBean
@Alternative()
public class AnotherOrdersBusinessService implements OrdersBusinessInterface {

	//Class Scoped Properties
	List<Order> orders;
		
    /**
     * Default constructor. 
     */
    public AnotherOrdersBusinessService() {
		// Implement dummy order list
		// Array of dummy orders.
		Order[] dummyOrders = new Order[] { new Order("E1432", "Yizino TV", 159.99f, 1),
				new Order("LT221", "Acme Bat Suit", 29.99f, 1), new Order("BEV02", "Cherry Xola", 7.49f, 5),
				new Order("U5983", "Poxie Paper Bowls", 4.99f, 10)};

		// Initialize orders with a dummyOrder array
		orders = Stream.of(dummyOrders).collect(Collectors.toList());
    }

	/**
     * @see OrdersBusinessInterface#test()
     */
    public void test() {
        System.out.println("Hello from AnotherOrdersBusinessService");
    }

    /**
     * @return List<Order> Orders
     */
	public List<Order> getOrders() {
		return this.orders;
	}

	/**
	 * @param List<Order> orders to set List<Order> orders
	 */
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	@Override
	public void sendOrder(Order order) {
		// TODO Auto-generated method stub
		
	}

}
